function createCard(name, description, pictureUrl, startDate, endDate, location) {
  return `
    <div class="card shadow-lg mt-3">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
        <div class="card-footer">${startDate} - ${endDate}</div>
    </div>
  </div>
  `;
}

function createPlaceholder() {
  return `
  <div class="card shadow-lg mt-3" aria-hidden="true" id="placeholder">
    <div class="placeholder-wave">
      <img src="https://placehold.co/600x400" class="card-img-top placeholder col-4" alt="...">
    </div>
    <div class="card-body">
      <h5 class="card-title placeholder-glow">
        <span class="placeholder col-4"></span>
      </h5>
      <div class="card-footer placeholder-glow">
        <span class="placeholder col-6">
      </div>
    </div>
  </div>
  `;
}

function throwError(e) {
  return `
  <div class="alert alert-danger" role="alert">${e}</div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw new Error('Response not ok');

      } else {
        const data = await response.json();

        let i = 0;
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            let html = createPlaceholder();
            let column = document.querySelector(`#col-${i%3}`);
            column.innerHTML += html;

            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = (new Date(details.conference.starts)).toLocaleDateString();
            const endDate = (new Date(details.conference.ends)).toLocaleDateString();
            const location = details.conference.location.name;
            html = createCard(title, description, pictureUrl, startDate, endDate, location);
            column = document.querySelector(`#col-${i%3}`);
            column.innerHTML += html;
            // remove placeholder card after createCard() execution
            const placeholder = document.getElementById('placeholder');
            placeholder.remove();
            i++
          }
        }
      }
    } catch (e) {
      console.error(e);
      const errorMessage = throwError(e);
      const message = document.querySelector('#card_error');
      message.innerHTML += errorMessage;
    }

  });
